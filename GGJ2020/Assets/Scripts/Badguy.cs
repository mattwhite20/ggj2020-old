﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Badguy : MonoBehaviour
{
    NavMeshAgent myNMA;

    // Start is called before the first frame update
    void Start()
    {
        myNMA = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!myNMA.hasPath)
        {
            ChooseTarget();
        }
    }

    void ChooseTarget()
    {
        int ranNum = Random.Range(0, Building.activeBuildings.Count);
        myNMA.SetDestination(Building.activeBuildings[ranNum].transform.position);
    }
}
