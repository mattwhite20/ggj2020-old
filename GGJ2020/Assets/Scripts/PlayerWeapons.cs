﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapons : MonoBehaviour
{
    [SerializeField] ParticleSystem bulletParticles;

    // Update is called once per frame
    void Update()
    {
        ReceiveInputs();
    }
    void ReceiveInputs()
    {
        if (Input.GetButton("Fire1"))
        {
            enableBullets(true);
        } else
        {
            enableBullets(false);
        }
    }
    void enableBullets(bool isActive)
    {
        var bulletEmission = bulletParticles.emission;
        bulletEmission.enabled = isActive;
    }
}
