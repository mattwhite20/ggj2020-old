﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyBuilding : MonoBehaviour
{
    [SerializeField] int hitsToDestroy = 3;
    [SerializeField] GameObject deathFX;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnParticleCollision(GameObject other)
    {
        hitsToDestroy--;
        if (hitsToDestroy < 1)
        {
            Destroy(gameObject);
        }
    }
}
